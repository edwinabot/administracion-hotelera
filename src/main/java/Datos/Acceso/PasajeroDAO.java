package Datos.Acceso;

import Datos.Modelo.Pasajero;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by edwin on 11/01/16.
 */
public class PasajeroDAO extends DAO<Pasajero, Integer> {

    public PasajeroDAO() throws Exception {
    }

    @Override
    public Pasajero insert(Pasajero registro) throws Exception {
        /*
        Con String.format() reemplazo los placeholders por los valores que necesito, estos valores se ponen en
        el orden en que están los placeholders en el string. Uso Locale.ENGLISH porque es la codificación que
        usa MySQL
        */
        String q = "INSERT INTO `tp_admin_hotelera`.`pasajero` " +
                "(`dni`,  `nombre`,  `apellido`,  `telefono`, `domicilio`,  `lista_negra`) " +
                "VALUES  (%d, '%s', '%s', '%s', '%s', %b);";
        q = String.format(Locale.ENGLISH, q, registro.getDni(), registro.getNombre(), registro.getApellido(),
                registro.getTelefono(), registro.getDomicilio(), registro.getListaNegra());

        // Luego me conecto, corro las expresiones y me desconecto
        this.bd.conectar();
        this.bd.ejecutarDML(q);
        registro = this.select(registro.getDni());
        this.bd.desconectar();
        // Devuelvo el registro con el codigo.
        return registro;
    }

    @Override
    public Pasajero update(Pasajero registro) throws Exception {
        String q = "UPDATE `tp_admin_hotelera`.`pasajero` " +
                "SET " +
                "`dni` = %d, " +
                "`nombre` = '%s', " +
                "`apellido` = '%s', " +
                "`telefono` = '%s', " +
                "`domicilio` = '%s', " +
                "`lista_negra` = %b " +
                "WHERE `dni` = %d;";
        q = String.format(Locale.ENGLISH, q, registro.getDni(), registro.getNombre(), registro.getApellido(),
                registro.getTelefono(), registro.getDomicilio(), registro.getListaNegra(), registro.getDni());
        this.bd.conectar();
        this.bd.ejecutarDML(q);
        registro = this.select(registro.getDni());
        this.bd.desconectar();
        return registro;
    }

    @Override
    public Pasajero delete(Pasajero registro) throws Exception {
        String q = "DELETE FROM `tp_admin_hotelera`.`pasajero` WHERE `dni` = %d;";
        q = String.format(Locale.ENGLISH, q, registro.getDni());
        this.bd.conectar();
        this.bd.ejecutarDML(q);
        this.bd.desconectar();
        return registro;
    }

    @Override
    public Pasajero select(Integer integer) throws Exception {
        String q = "SELECT * FROM `tp_admin_hotelera`.`pasajero` WHERE `dni` = %d;";
        q = String.format(Locale.ENGLISH, q, integer);
        this.bd.conectar();
        this.bd.ejecutarDQL(q);
        Pasajero registro = new Pasajero();
        ResultSet resultado = this.bd.getResultset();
        while (resultado.next()) {
            // Verifico desde el primero si es el último registro para reforzar la condición de
            // que solo se recupere un objeto para ese id.
            // Probablemente inútil porque la bd tiene PKs simples y, por supuesto, las PK
            // son únicas.
            if (!resultado.isLast()) {
                throw new Exception("El id provisto tiene mas de un resultado");
            }
            registro.setDni(resultado.getInt("dni"));
            registro.setNombre(resultado.getString("nombre"));
            registro.setApellido(resultado.getString("apellido"));
            registro.setDomicilio(resultado.getString("domicilio"));
            registro.setListaNegra(resultado.getBoolean("lista_negra"));
            registro.setTelefono(resultado.getString("telefono"));
        }
        this.bd.desconectar();
        return registro;
    }

    @Override
    public ArrayList<Pasajero> selectTodo() throws Exception {
        String q = "SELECT * FROM `tp_admin_hotelera`.`pasajero`;";
        q = String.format(Locale.ENGLISH, q);
        this.bd.conectar();
        this.bd.ejecutarDQL(q);
        ArrayList<Pasajero> array_resultado = new ArrayList<Pasajero>();
        ResultSet resultado = this.bd.getResultset();
        while (resultado.next()) {
            Pasajero registro = new Pasajero();
            registro.setDni(resultado.getInt("dni"));
            registro.setNombre(resultado.getString("nombre"));
            registro.setApellido(resultado.getString("apellido"));
            registro.setDomicilio(resultado.getString("domicilio"));
            registro.setListaNegra(resultado.getBoolean("lista_negra"));
            registro.setTelefono(resultado.getString("telefono"));
            array_resultado.add(registro);
        }
        this.bd.desconectar();
        return array_resultado;
    }
}
