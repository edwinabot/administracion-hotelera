package Datos.Acceso;

import Datos.Modelo.Producto;
import Datos.Modelo.Reserva;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by edwin on 11/01/16.
 */
public class ReservaDAO extends DAO<Reserva, Integer> {

    public ReservaDAO() throws Exception {
    }

    @Override
    public Reserva insert(Reserva registro) throws Exception {
        String insert = "INSERT INTO `tp_admin_hotelera`.`reserva` " +
                "(`confirmada`, `acompaniantes`, `checkin`, `checkout`, " +
                "`huesped`, `habitacion`) VALUES (%b, %d, '%tF', '%tF', %d, %d);";
        insert = String.format(Locale.ENGLISH, insert, registro.getConfirmada(), registro.getAcompaniantes(),
                registro.getCheckin(), registro.getCheckout(), registro.getHuesped().getDni(),
                registro.getHabitacion().getNumero());
        bd.conectar();
        bd.ejecutarDML(insert);
        bd.ejecutarDQL("SELECT MAX(`numero`) as 'numero' FROM `tp_admin_hotelera`.`reserva`");
        ResultSet r = bd.getResultset();
        r.first();
        registro.setNumero(r.getInt("numero"));
        bd.desconectar();
        return registro;
    }

    @Override
    public Reserva update(Reserva registro) throws Exception {
        String insert = "UPDATE `tp_admin_hotelera`.`reserva`\n" +
                "SET\n" +
                "`numero` = %d,\n" +
                "`confirmada` = %b,\n" +
                "`acompaniantes` = %d,\n" +
                "`checkin` = '%tF',\n" +
                "`checkout` = '%tF',\n" +
                "`huesped` = %d,\n" +
                "`habitacion` = %d\n" +
                "WHERE `numero` = %d;";
        insert = String.format(Locale.ENGLISH, insert, registro.getNumero(), registro.getConfirmada(), registro.getAcompaniantes(),
                registro.getCheckin(), registro.getCheckout(), registro.getHuesped().getDni(),
                registro.getHabitacion().getNumero(), registro.getNumero());
        bd.conectar();
        bd.ejecutarDML(insert);
        bd.desconectar();
        return registro;
    }

    @Override
    public Reserva delete(Reserva registro) throws Exception {
        String del = "DELETE FROM `tp_admin_hotelera`.`reserva`\n" +
                "WHERE %d;";
        del = String.format(Locale.ENGLISH, del, registro.getNumero());
        bd.conectar();
        bd.ejecutarDML(del);
        bd.desconectar();
        return registro;
    }

    @Override
    public Reserva select(Integer integer) throws Exception {
        String q = "SELECT * FROM reserva WHERE numero = %d;";
        q = String.format(Locale.ENGLISH, q, integer);
        bd.conectar();
        ResultSet resultSet = bd.ejecutarDQL(q);
        resultSet.first();
        Reserva reserva = new Reserva();
        reserva.setNumero(resultSet.getInt("numero"));
        reserva.setConfirmada(resultSet.getBoolean("confirmada"));
        reserva.setAcompaniantes(resultSet.getInt("acompaniantes"));
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        reserva.setCheckin(df.parse(resultSet.getString("checkin")));
        reserva.setCheckout(df.parse(resultSet.getString("checkout")));
        reserva.setHabitacion(new HabitacionDAO().select(resultSet.getInt("habitacion")));
        reserva.setHuesped(new PasajeroDAO().select(resultSet.getInt("huesped")));
        bd.desconectar();
        reserva = this.selectAllConsumos(reserva);
        return reserva;
    }

    @Override
    public ArrayList<Reserva> selectTodo() throws Exception {
        String q = "SELECT * FROM reserva;";
        bd.conectar();
        ResultSet resultSet = bd.ejecutarDQL(q);
        ArrayList<Reserva> resultado = new ArrayList<Reserva>();
        while (resultSet.next()) {
            Reserva reserva = new Reserva();
            reserva.setNumero(resultSet.getInt("numero"));
            reserva.setConfirmada(resultSet.getBoolean("confirmada"));
            reserva.setAcompaniantes(resultSet.getInt("acompaniantes"));
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            reserva.setCheckin(df.parse(resultSet.getString("checkin")));
            reserva.setCheckout(df.parse(resultSet.getString("checkout")));
            reserva.setHabitacion(new HabitacionDAO().select(resultSet.getInt("habitacion")));
            reserva.setHuesped(new PasajeroDAO().select(resultSet.getInt("huesped")));
            resultado.add(reserva);
        }
        bd.desconectar();
        return resultado;
    }

    public Reserva selectAllConsumos(Reserva reserva) throws Exception {
        String q = "SELECT `producto` FROM `tp_admin_hotelera`.`reserva_has_producto` WHERE `reserva` = %d;";
        q = String.format(Locale.ENGLISH, q, reserva.getNumero());
        bd.conectar();
        ResultSet resultSet = bd.ejecutarDQL(q);
        ProductoDAO pdao = new ProductoDAO();
        while (resultSet.next()) {
            Producto p = pdao.select(resultSet.getInt("producto"));
            reserva.getConsumos().add(p);
        }
        bd.desconectar();
        return reserva;
    }

    public Reserva insertConsumo(Producto producto, Reserva reserva) throws Exception {
        String q = "INSERT INTO `tp_admin_hotelera`.`reserva_has_producto` (`reserva`, `producto`) VALUES (%d, %d);";
        q = String.format(Locale.ENGLISH, q, reserva.getNumero(), producto.getCodigo());
        bd.conectar();
        bd.ejecutarDML(q);
        bd.desconectar();
        reserva.getConsumos().add(producto);
        return reserva;
    }

    public Reserva deleteConsumo(Producto producto, Reserva reserva) throws Exception {
        String q = "DELETE FROM `tp_admin_hotelera`.`reserva_has_producto` " +
                "WHERE `reserva` = %d AND `producto` = %d LIMIT 1;";
        q = String.format(Locale.ENGLISH, q, reserva.getNumero(), producto.getCodigo());
        bd.conectar();
        bd.ejecutarDML(q);
        bd.desconectar();
        reserva.getConsumos().remove(producto);
        return reserva;
    }
}
