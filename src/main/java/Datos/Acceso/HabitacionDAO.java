package Datos.Acceso;

import Datos.Modelo.Habitacion;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by edwin on 11/01/16.
 */
public class HabitacionDAO extends DAO<Habitacion, Integer> {

    public HabitacionDAO() throws Exception {
    }

    @Override
    public Habitacion insert(Habitacion registro) throws Exception {
        /*
        Con String.format() reemplazo los placeholders por los valores que necesito, estos valores se ponen en
        el orden en que están los placeholders en el string. Uso Locale.ENGLISH porque es la codificación que
        usa MySQL
        */
        String q = "INSERT INTO `tp_admin_hotelera`.`habitacion`" +
                "(`numero`, `descripcion`, `tarifa`, `limpiar`, `mantenimiento`, `ocupada`) " +
                "VALUES (%d, '%s', %f, %b, %b, %b);";
        q = String.format(Locale.ENGLISH, q, registro.getNumero(), registro.getDescripcion(), registro.getTarifa(),
                registro.getLimpiar(), registro.getMantenimiento(), registro.getOcupada());

        // Luego me conecto, corro las expresiones y me desconecto
        this.bd.conectar();
        this.bd.ejecutarDML(q);
        registro = this.select(registro.getNumero());
        this.bd.desconectar();
        // Devuelvo el registro con el codigo.
        return registro;
    }

    @Override
    public Habitacion update(Habitacion registro) throws Exception {
        String q = "UPDATE `tp_admin_hotelera`.`habitacion` " +
                "SET " +
                "`numero` = %d, " +
                "`descripcion` = '%s', " +
                "`tarifa` = %f, " +
                "`limpiar` = %b, " +
                "`mantenimiento` = %b, " +
                "`ocupada` = %b " +
                "WHERE `numero` = %d;";
        q = String.format(Locale.ENGLISH, q, registro.getNumero(), registro.getDescripcion(), registro.getTarifa(),
                registro.getLimpiar(), registro.getMantenimiento(), registro.getOcupada(), registro.getNumero());
        this.bd.conectar();
        this.bd.ejecutarDML(q);
        registro = this.select(registro.getNumero());
        this.bd.desconectar();
        return registro;
    }

    @Override
    public Habitacion delete(Habitacion registro) throws Exception {
        String q = "DELETE FROM `tp_admin_hotelera`.`habitacion` WHERE `numero` = %d;";
        q = String.format(Locale.ENGLISH, q, registro.getNumero());
        this.bd.conectar();
        this.bd.ejecutarDML(q);
        this.bd.desconectar();
        return registro;
    }

    @Override
    public Habitacion select(Integer integer) throws Exception {
        String q = "SELECT * FROM `tp_admin_hotelera`.`habitacion` WHERE `numero` = %d;";
        q = String.format(Locale.ENGLISH, q, integer);
        this.bd.conectar();
        this.bd.ejecutarDQL(q);
        Habitacion registro = new Habitacion();
        ResultSet resultado = this.bd.getResultset();
        while (resultado.next()) {
            // Verifico desde el primero si es el último registro para reforzar la condición de
            // que solo se recupere un objeto para ese id.
            // Probablemente inútil porque la bd tiene PKs simples y, por supuesto, las PK
            // son únicas.
            if (!resultado.isLast()) {
                throw new Exception("El id provisto tiene mas de un resultado");
            }
            registro.setNumero(resultado.getInt("numero"));
            registro.setDescripcion(resultado.getString("descripcion"));
            registro.setTarifa(resultado.getDouble("tarifa"));
            registro.setLimpiar(resultado.getBoolean("limpiar"));
            registro.setMantenimiento(resultado.getBoolean("mantenimiento"));
            registro.setOcupada(resultado.getBoolean("ocupada"));
        }
        this.bd.desconectar();
        return registro;
    }

    @Override
    public ArrayList<Habitacion> selectTodo() throws Exception {
        String q = "SELECT * FROM `tp_admin_hotelera`.`habitacion` ORDER BY `numero`;";
        this.bd.conectar();
        this.bd.ejecutarDQL(q);
        ArrayList<Habitacion> array_resultado = new ArrayList<Habitacion>();
        ResultSet resultado = this.bd.getResultset();
        while (resultado.next()) {
            Habitacion h = new Habitacion();
            h.setNumero(resultado.getInt("numero"));
            h.setDescripcion(resultado.getString("descripcion"));
            h.setTarifa(resultado.getDouble("tarifa"));
            h.setLimpiar(resultado.getBoolean("limpiar"));
            h.setMantenimiento(resultado.getBoolean("mantenimiento"));
            h.setOcupada(resultado.getBoolean("ocupada"));
            array_resultado.add(h);
        }
        this.bd.desconectar();
        return array_resultado;
    }
}
