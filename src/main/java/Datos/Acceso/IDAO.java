package Datos.Acceso;

import java.util.ArrayList;

/**
 * Created by edwin on 07/01/16.
 */
public interface IDAO<T, PK> {
    /***
     * Inserta un objeto en la base de datos
     * @param registro
     * @return
     * @throws Exception
     */
    T insert(T registro) throws Exception;

    /***
     * Actualiza un objeto en la base de datos
     * @param registro
     * @return
     * @throws Exception
     */
    T update(T registro) throws Exception;

    /***
     * Elimina un objeto de la base de datos
     * @param registro
     * @return
     * @throws Exception
     */
    T delete(T registro) throws Exception;

    /***
     * Recupera un objeto según a partir de su PK
     * @param pk
     * @return
     * @throws Exception
     */
    T select(PK pk) throws Exception;

    /***
     * Recupera todos los objetos
     * @return
     * @throws Exception
     */
    ArrayList<T> selectTodo() throws Exception;
}
