package Datos.Acceso;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.sql.*;

/**
 * Created by edwin on 05/01/16.
 */
public class BaseDatosMySQL {
    private String usuario, contrasenia, url;
    private Connection con;
    private ResultSet resultset;
    private Integer restultcod;

    /***
     * Levanta las configuraciones de la base de datos de un archivo de configuracion XML (configuraciones.xml)
     * @throws java.lang.Exception
     */
    public BaseDatosMySQL() throws Exception {
        super();
        this.verificarInstalacionDriver();
        this.leerConfiguracionXML();
    }

    private void leerConfiguracionXML() throws Exception {
        try {
            // Abro un archivo
            File fXmlFile = new File(getClass().getResource("/configuraciones.xml").toURI());

            // Creo un objeto para leer el árbol de nodos XML
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            // Creo un objeto para reconstruir el documento XML
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            // Parseo el archivo xml con contructor de documentos y así obtengo un documento xml
            Document doc = dBuilder.parse(fXmlFile);

            // Opcional pero recomendado
            // http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            // Normalizo el documento para optimizar y evitar errores.
            doc.getDocumentElement().normalize();

            // Busco el elemento base_datos
            NodeList nList = doc.getElementsByTagName("base_datos");

            // Los objetos NodeList no se pueden recorrer con la sintaxis 'Objeto n:NodeList' así que creo
            // el bucle con un contador
            for (int temp = 0; temp < nList.getLength(); temp++) {
                // Creo un Nodo
                Node nNode = nList.item(temp);

                // Verifico que sea un nodo y a su vez que sea un nodo base_datos
                if (nNode.getNodeType() == Node.ELEMENT_NODE && nNode.getNodeName().equals("base_datos")) {
                    // Construyo el elemento
                    Element eElement = (Element) nNode;
                    // Leo los hijos del elemento
                    this.usuario = eElement.getElementsByTagName("user").item(0).getTextContent();
                    this.contrasenia = eElement.getElementsByTagName("password").item(0).getTextContent();
                    this.url = eElement.getElementsByTagName("url").item(0).getTextContent();
                    // Salgo del bucle
                    break;
                }
            }

        } catch (Exception e) {
            throw e;
        }
    }

    private void verificarInstalacionDriver() throws ClassNotFoundException {
        //Pruebo que este instalado el driver de MySQL
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (java.lang.ClassNotFoundException e) {
            System.err.print("ClassNotFoundException: ");
            System.err.println(e.getMessage());
            throw e;
        }
    }

    /***
     * Abre una sesion en la base de datos.
     *
     * @return Nada
     */
    public void conectar() throws SQLException {
        // Armo la coneccion
        try {
            this.con = DriverManager.getConnection(this.url, this.usuario, this.contrasenia);
        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            throw ex;
        }
    }

    /***
     * Cierra las sesiones abiertas en la base de datos.
     *
     * @throws Exception
     */
    public void desconectar() throws Exception {
        try {
            if (this.getResultset() != null) {
                this.getResultset().close();
            }
            if (this.con != null) {
                this.con.close();
            }
        } catch (Exception e) {
            System.err.print("Exception: ");
            System.err.println(e.getMessage());
            throw e;
        }
    }

    /***
     * Ejecuta un Statement con DQL
     *
     * @param exp es una expresión sql
     * @return
     * @throws SQLException
     */
    public ResultSet ejecutarDQL(String exp) throws SQLException {
        // Un objeto Statement es el que permite realizar SQL queries a la base de datos
        Statement statement = this.con.createStatement();
        // Un objeto ResultSet es el que contiene un resultset de un query
        this.resultset = statement.executeQuery(exp);
        return this.getResultset();
    }

    /***
     * Ejecuta un Statement con DML
     *
     * @param exp es una expresión sql
     * @return
     * @throws SQLException
     */
    public Integer ejecutarDML(String exp) throws SQLException {
        // Un objeto Statement es el que permite realizar SQL queries a la base de datos
        Statement statement = this.con.createStatement();
        // Un objeto ResultSet es el que contiene un resultset de un query
        this.restultcod = statement.executeUpdate(exp);
        return this.getRestultcod();
    }



    /***
     * Verifica si la sesion esta abierta
     *
     * @return
     * @throws Exception
     */
    public Boolean conexionEstaAbierta() throws Exception {
        try {
            return !this.con.isClosed();
        } catch (SQLException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultSet getResultset() {
        return resultset;
    }

    public Integer getRestultcod() {
        return restultcod;
    }
}
