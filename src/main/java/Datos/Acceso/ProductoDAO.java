package Datos.Acceso;

import Datos.Modelo.Producto;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by edwin on 07/01/16.
 */
public class ProductoDAO extends DAO<Producto, Integer> {

    public ProductoDAO() throws Exception {
        super();
    }

    @Override
    public Producto insert(Producto registro) throws Exception {
        /*
        Con String.format() reemplazo los placeholders por los valores que necesito, estos valores se ponen en
        el orden en que están los placeholders en el string. Uso Locale.ENGLISH porque es la codificación que
        usa MySQL
        */
        String q = "INSERT INTO `producto`(`descripcion`,`precio`)VALUES('%s', %f);";
        q = String.format(Locale.ENGLISH, q, registro.getDescripcion(), registro.getPrecio());

        // Luego me conecto, corro las expresiones y me desconecto
        this.bd.conectar();
        this.bd.ejecutarDML(q);
        registro = this.select(registro.getDescripcion());
        this.bd.desconectar();
        // Devuelvo el registro con el codigo.
        return registro;
    }

    @Override
    public Producto update(Producto registro) throws Exception {
        String q = "UPDATE `producto` SET `descripcion` = '%s', `precio` = %f WHERE `codigo` = %d;";
        q = String.format(Locale.ENGLISH, q, registro.getDescripcion(), registro.getPrecio(), registro.getCodigo());
        this.bd.conectar();
        this.bd.ejecutarDML(q);
        registro = this.select(registro.getCodigo());
        this.bd.desconectar();
        return registro;
    }

    @Override
    public Producto delete(Producto registro) throws Exception {
        String q = "DELETE FROM `producto` WHERE `codigo` = %d;";
        q = String.format(Locale.ENGLISH, q, registro.getCodigo());
        this.bd.conectar();
        this.bd.ejecutarDML(q);
        this.bd.desconectar();
        return registro;
    }

    @Override
    public Producto select(Integer pk) throws Exception {
        String q = "SELECT * FROM `producto` WHERE `codigo` = %d;";
        q = String.format(Locale.ENGLISH, q, pk);
        this.bd.conectar();
        this.bd.ejecutarDQL(q);
        Producto registro = new Producto();
        ResultSet resultado = this.bd.getResultset();
        while (resultado.next()) {
            // Verifico desde el primero si es el último registro para reforzar la condición de
            // que solo se recupere un objeto para ese id.
            // Probablemente inútil porque la bd tiene PKs simples y, por supuesto, las PK
            // son únicas.
            if (!resultado.isLast()) {
                throw new Exception("El id provisto tiene mas de un resultado");
            }
            registro.setCodigo(resultado.getInt("codigo"));
            registro.setDescripcion(resultado.getString("descripcion"));
            registro.setPrecio(resultado.getDouble("precio"));
        }
        this.bd.desconectar();
        return registro;
    }

    public Producto select(String descripcion) throws Exception {
        String q = "SELECT * FROM `producto` WHERE `descripcion` = '%s';";
        q = String.format(Locale.ENGLISH, q, descripcion);
        this.bd.conectar();
        this.bd.ejecutarDQL(q);
        Producto registro = new Producto();
        ResultSet resultado = this.bd.getResultset();
        while (resultado.next()) {
            // Verifico desde el primero si es el último registro para reforzar la condición de
            // que solo se recupere un objeto para ese id.
            // Probablemente inútil porque la bd tiene PKs simples y, por supuesto, las PK
            // son únicas.
            if (!resultado.isLast()) {
                throw new Exception("El id provisto tiene mas de un resultado");
            }
            registro.setCodigo(resultado.getInt("codigo"));
            registro.setDescripcion(resultado.getString("descripcion"));
            registro.setPrecio(resultado.getDouble("precio"));
        }
        this.bd.desconectar();
        return registro;
    }

    @Override
    public ArrayList<Producto> selectTodo() throws Exception {
        String q = "SELECT * FROM `producto` ORDER BY `descripcion`;";
        this.bd.conectar();
        this.bd.ejecutarDQL(q);
        ArrayList<Producto> array_resultado = new ArrayList<Producto>();
        ResultSet resultado = this.bd.getResultset();
        while (resultado.next()) {
            Producto p = new Producto();
            p.setCodigo(resultado.getInt("codigo"));
            p.setDescripcion(resultado.getString("descripcion"));
            p.setPrecio(resultado.getDouble("precio"));
            array_resultado.add(p);
        }
        this.bd.desconectar();
        return array_resultado;
    }
}
