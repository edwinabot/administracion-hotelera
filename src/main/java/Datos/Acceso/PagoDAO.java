package Datos.Acceso;

import Datos.Modelo.Pago;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by edwin on 12/01/16.
 */
public class PagoDAO extends DAO<Pago, Integer> {
    public PagoDAO() throws Exception {
    }

    @Override
    public Pago insert(Pago registro) throws Exception {
        String ins = "INSERT INTO `tp_admin_hotelera`.`pago`\n" +
                "(`fecha`,\n" +
                "`importe`,\n" +
                "`medio`,\n" +
                "`reserva`)\n" +
                "VALUES\n" +
                "('%tF',\n" +
                "'%f',\n" +
                "'%s',\n" +
                "%d);";
        ins = String.format(Locale.ENGLISH, ins, registro.getFecha(), registro.getImporte(), registro.getMedio(),
                registro.getReserva().getNumero());
        bd.conectar();
        Integer rc = bd.ejecutarDML(ins);
        bd.ejecutarDQL("SELECT MAX(`numero`) as 'numero' FROM `tp_admin_hotelera`.`pago`");
        ResultSet r = bd.getResultset();
        r.first();
        registro.setNumero(r.getInt("numero"));
        bd.desconectar();
        return registro;
    }

    @Override
    public Pago update(Pago registro) throws Exception {
        String upd = "UPDATE `tp_admin_hotelera`.`pago`\n" +
                "SET\n" +
                "`fecha` = '%tF',\n" +
                "`importe` = %f,\n" +
                "`medio` = '%s',\n" +
                "`reserva` = %d\n" +
                "WHERE `numero` = %d;\n";
        upd = String.format(Locale.ENGLISH, upd, registro.getFecha(), registro.getImporte(), registro.getMedio(),
                registro.getReserva().getNumero(), registro.getNumero());
        bd.conectar();
        Integer rc = bd.ejecutarDML(upd);
        bd.desconectar();
        return registro;
    }

    @Override
    public Pago delete(Pago registro) throws Exception {
        String del = "DELETE FROM `tp_admin_hotelera`.`pago`\n" +
                "WHERE `numero` = %d;";
        del = String.format(Locale.ENGLISH, del, registro.getNumero());
        bd.conectar();
        Integer rc = bd.ejecutarDML(del);
        bd.desconectar();
        return registro;
    }

    @Override
    public Pago select(Integer integer) throws Exception {
        String sel = "SELECT *\n" +
                "FROM `tp_admin_hotelera`.`pago`\n" +
                "WHERE `numero` = %d;";
        sel = String.format(Locale.ENGLISH, sel, integer);
        bd.conectar();
        ResultSet resultSet = bd.ejecutarDQL(sel);
        resultSet.first();
        Pago pago = new Pago();
        pago.setNumero(resultSet.getInt("numero"));
        pago.setFecha(resultSet.getString("fecha"));
        pago.setImporte(resultSet.getDouble("importe"));
        pago.setMedio(resultSet.getString("medio"));
        pago.setReserva(new ReservaDAO().select(resultSet.getInt("reserva")));
        bd.desconectar();
        return pago;
    }

    @Override
    public ArrayList<Pago> selectTodo() throws Exception {
        String sel = "SELECT *\n" +
                "FROM `tp_admin_hotelera`.`pago`;";
        bd.conectar();
        ResultSet resultSet = bd.ejecutarDQL(sel);
        ArrayList<Pago> res = new ArrayList<Pago>();
        while (resultSet.next()) {
            Pago pago = new Pago();
            pago.setNumero(resultSet.getInt("numero"));
            pago.setFecha(resultSet.getString("fecha"));
            pago.setImporte(resultSet.getDouble("importe"));
            pago.setMedio(resultSet.getString("medio"));
            pago.setReserva(new ReservaDAO().select(resultSet.getInt("reserva")));
            res.add(pago);
        }
        bd.desconectar();
        return res;
    }
}
