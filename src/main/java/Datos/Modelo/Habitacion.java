package Datos.Modelo;

/**
 * Created by edwin on 05/01/16.
 */
public class Habitacion {
    private Integer numero;
    private String descripcion;
    private Double tarifa;
    private Boolean limpiar;
    private Boolean mantenimiento;
    private Boolean ocupada;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getTarifa() {
        return tarifa;
    }

    public void setTarifa(Double tarifa) {
        this.tarifa = tarifa;
    }

    public Boolean getLimpiar() {
        return limpiar;
    }

    public void setLimpiar(Boolean limpiar) {
        this.limpiar = limpiar;
    }

    public Boolean getMantenimiento() {
        return mantenimiento;
    }

    public void setMantenimiento(Boolean mantenimiento) {
        this.mantenimiento = mantenimiento;
    }

    public Boolean getOcupada() {
        return ocupada;
    }

    public void setOcupada(Boolean ocupada) {
        this.ocupada = ocupada;
    }

    @Override
    public String toString() {
        return "Habitacion{" +
                "numero=" + numero +
                ", descripcion='" + descripcion + '\'' +
                ", tarifa=" + tarifa +
                ", limpiar=" + limpiar +
                ", mantenimiento=" + mantenimiento +
                ", ocupada=" + ocupada +
                '}';
    }
}
