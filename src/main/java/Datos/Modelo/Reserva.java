package Datos.Modelo;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by edwin on 05/01/16.
 */
public class Reserva {
    private Integer numero;
    private Habitacion habitacion;
    private Pasajero huesped;
    private Boolean confirmada;
    private Integer acompaniantes;
    private Date checkin;
    private Date checkout;
    private ArrayList<Pago> pagos;
    private ArrayList<Producto> consumos;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Habitacion getHabitacion() {
        return habitacion;
    }

    public void setHabitacion(Habitacion habitacion) {
        this.habitacion = habitacion;
    }

    public Pasajero getHuesped() {
        return huesped;
    }

    public void setHuesped(Pasajero huesped) {
        this.huesped = huesped;
    }

    public Boolean getConfirmada() {
        return confirmada;
    }

    public void setConfirmada(Boolean confirmada) {
        this.confirmada = confirmada;
    }

    public Integer getAcompaniantes() {
        return acompaniantes;
    }

    public void setAcompaniantes(Integer acompaniantes) {
        this.acompaniantes = acompaniantes;
    }

    public Date getCheckin() {
        return checkin;
    }

    public void setCheckin(Date checkin) {
        this.checkin = checkin;
    }

    public Date getCheckout() {
        return checkout;
    }

    public void setCheckout(Date checkout) {
        this.checkout = checkout;
    }

    public ArrayList<Pago> getPagos() {
        return pagos;
    }

    public void setPagos(ArrayList<Pago> pagos) {
        this.pagos = pagos;
    }

    public ArrayList<Producto> getConsumos() {
        return consumos;
    }

    public void setConsumos(ArrayList<Producto> consumos) {
        this.consumos = consumos;
    }
}
