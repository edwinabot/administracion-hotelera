package Datos.Modelo;

/**
 * Created by edwin on 05/01/16.
 */
public class Pasajero {
    private String nombre;
    private String apellido;
    private Integer dni;
    private String telefono;
    private String domicilio;
    private Boolean listaNegra;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public Boolean getListaNegra() {
        return listaNegra;
    }

    public void setListaNegra(Boolean listaNegra) {
        this.listaNegra = listaNegra;
    }

    @Override
    public String toString() {
        return "nombre='" + nombre + '\'' +
               ", apellido='" + apellido + '\'' +
               ", dni=" + dni;
    }

}
