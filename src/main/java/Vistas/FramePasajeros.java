package Vistas;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListModel;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import Datos.Acceso.PasajeroDAO;
import Datos.Modelo.Pasajero;

import javax.swing.JButton;
import net.miginfocom.swing.MigLayout;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import java.awt.BorderLayout;

public class FramePasajeros extends JInternalFrame {
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtDni;
	private JTextField txtTelfono;
	private JTextField txtDomicilio;
	/**
	 * @wbp.nonvisual location=234,-3
	 */
	private final Component verticalGlue = Box.createVerticalGlue();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					FramePasajeros frame = new FramePasajeros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws Exception 
	 */
	public FramePasajeros() throws Exception {
		setClosable(true);
		setBounds(100, 100, 554, 419);
		getContentPane().setLayout(new MigLayout("", "[272px][272px]", "[387px]"));
		
		JPanel panelForm = new JPanel();
		getContentPane().add(panelForm, "cell 0 0,grow");
		panelForm.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		JLabel lblNombre = new JLabel("Nombre");
		panelForm.add(lblNombre, "2, 2, right, default");
		
		txtNombre = new JTextField();
		txtNombre.setText("Nombre");
		panelForm.add(txtNombre, "4, 2, fill, default");
		txtNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido");
		panelForm.add(lblApellido, "2, 4, right, default");
		
		txtApellido = new JTextField();
		txtApellido.setText("Apellido");
		panelForm.add(txtApellido, "4, 4, fill, default");
		txtApellido.setColumns(10);
		
		JLabel lblDni = new JLabel("DNI");
		panelForm.add(lblDni, "2, 6, right, default");
		
		txtDni = new JTextField();
		txtDni.setText("DNI");
		panelForm.add(txtDni, "4, 6, fill, default");
		txtDni.setColumns(10);
		
		JLabel lblTelfono = new JLabel("Teléfono");
		panelForm.add(lblTelfono, "2, 8, right, default");
		
		txtTelfono = new JTextField();
		txtTelfono.setText("Teléfono");
		panelForm.add(txtTelfono, "4, 8, fill, default");
		txtTelfono.setColumns(10);
		
		JLabel lblDomicilio = new JLabel("Domicilio");
		panelForm.add(lblDomicilio, "2, 10");
		
		txtDomicilio = new JTextField();
		txtDomicilio.setText("Domicilio");
		panelForm.add(txtDomicilio, "4, 10, fill, default");
		txtDomicilio.setColumns(10);
		
		JCheckBox chckbxListaNegra = new JCheckBox("Lista negra");
		panelForm.add(chckbxListaNegra, "4, 12");
		
		JButton btnGuardar = new JButton("Guardar");
		panelForm.add(btnGuardar, "4, 16");
		
		JPanel panelLista = new JPanel();
		getContentPane().add(panelLista, "cell 1 0,grow");
		panelLista.setLayout(new BorderLayout(0, 0));
		
		PasajeroDAO pdao = new PasajeroDAO();
		ArrayList<Pasajero> pasajeros = pdao.selectTodo();
		JList listPasajeros = new JList(pasajeros.toArray());
		listPasajeros.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		panelLista.add(listPasajeros, BorderLayout.CENTER);

	}

}
