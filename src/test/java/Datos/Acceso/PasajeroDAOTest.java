package Datos.Acceso;

import Datos.Modelo.Pasajero;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;

/**
 * Created by edwin on 12/01/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PasajeroDAOTest {

    @Test
    public void test_A_Insert() throws Exception {
        PasajeroDAO dao = new PasajeroDAO();
        Pasajero pas = new Pasajero();
        pas.setDni(12345678);
        pas.setNombre("Juan");
        pas.setApellido("Delos Palotes");
        pas.setDomicilio("Calle 1234");
        pas.setListaNegra(Boolean.FALSE);
        pas.setTelefono("555-4455");
        Pasajero insertado = dao.insert(pas);
        assert !(insertado == null);
    }

    @Test
    public void test_B_Select() throws Exception {
        PasajeroDAO dao = new PasajeroDAO();
        Pasajero pas = dao.select(12345678);
        assert !(pas == null) && (pas.getDni() == 12345678);
    }

    @Test
    public void test_C_SelectTodo() throws Exception {
        PasajeroDAO dao = new PasajeroDAO();
        ArrayList<Pasajero> pasajeros = dao.selectTodo();
        assert !(pasajeros.isEmpty());
    }

    @Test
    public void test_D_Update() throws Exception {
        PasajeroDAO dao = new PasajeroDAO();
        Pasajero pas = dao.select(12345678);
        pas.setListaNegra(Boolean.TRUE);
        Pasajero updated = dao.update(pas);
        assert updated.getDni().equals(pas.getDni()) && updated.getListaNegra();
    }

    @Test
    public void test_E_Delete() throws Exception {
        PasajeroDAO dao = new PasajeroDAO();
        Pasajero pas = dao.select(12345678);
        dao.delete(pas);
    }
}