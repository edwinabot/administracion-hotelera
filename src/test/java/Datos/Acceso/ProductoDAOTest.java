package Datos.Acceso;

import Datos.Modelo.Producto;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;

/**
 * Created by edwin on 11/01/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductoDAOTest {


    @Test
    public void test_A_Insert() throws Exception {
        ProductoDAO pdao = new ProductoDAO();
        Producto prod = new Producto();
        prod.setDescripcion("Producto ejemplo");
        prod.setPrecio(125.36);
        Producto insertado = pdao.insert(prod);
        assert prod.getCodigo() == null && insertado.getCodigo() != null;
    }

    @Test
    public void test_B_SelectTodo() throws Exception {
        ProductoDAO pdao = new ProductoDAO();
        ArrayList<Producto> productos = pdao.selectTodo();
        assert !productos.isEmpty();
    }

    @Test
    public void test_C_SelectPK() throws Exception {
        ProductoDAO pdao = new ProductoDAO();
        ArrayList<Producto> productos = pdao.selectTodo();
        Producto p = pdao.select(productos.get(0).getCodigo());
        assert !productos.isEmpty() && p.getCodigo() == productos.get(0).getCodigo();
    }

    @Test
    public void test_D_SelectDescripcion() throws Exception {
        ProductoDAO pdao = new ProductoDAO();
        String descripcion = "Producto ejemplo";
        Producto prod = pdao.select(descripcion);
        assert prod.getCodigo() != null && prod.getDescripcion().equals(descripcion);
    }

    @Test
    public void test_E_Update() throws Exception {
        ProductoDAO pdao = new ProductoDAO();
        ArrayList<Producto> productos = pdao.selectTodo();
        Producto p = null;
        for (Producto aux : productos) {
            if (aux.getDescripcion().equals("Producto ejemplo")) {
                p = aux;
                break;
            }
        }
        p.setDescripcion("Producto ejemplo actualizado");
        Producto q = pdao.update(p);
        assert q.getCodigo() == p.getCodigo();
    }

    @Test
    public void test_F_Delete() throws Exception {
        ProductoDAO pdao = new ProductoDAO();
        ArrayList<Producto> productos = pdao.selectTodo();
        Producto p = null;
        for (Producto aux : productos) {
            if (aux.getDescripcion().equals("Producto ejemplo actualizado")) {
                p = aux;
                break;
            }
        }
        pdao.delete(p);
    }
}