package Datos.Acceso;

import Datos.Modelo.Pago;
import Datos.Modelo.Reserva;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by edwin on 12/01/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PagoDAOTest {
    private static Pago pago = new Pago();
    private static Reserva reserva = new Reserva();

    @BeforeClass
    public static void setUp() throws Exception {
        PagoDAOTest.reserva.setConfirmada(Boolean.FALSE);
        PagoDAOTest.reserva.setAcompaniantes(2);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        PagoDAOTest.reserva.setCheckin(df.parse("2016-01-10"));
        PagoDAOTest.reserva.setCheckout(df.parse("2016-01-16"));
        PagoDAOTest.reserva.setHabitacion(new HabitacionDAO().selectTodo().get(0));
        PagoDAOTest.reserva.setHuesped(new PasajeroDAO().selectTodo().get(0));
        ReservaDAO dao = new ReservaDAO();
        PagoDAOTest.pago.setFecha("2016-12-23");
        PagoDAOTest.pago.setImporte(258.22);
        PagoDAOTest.pago.setMedio("Cash");
        PagoDAOTest.pago.setReserva(dao.insert(PagoDAOTest.reserva));
    }

    @AfterClass
    public static void tearDown() throws Exception {
        ReservaDAO dao = new ReservaDAO();
        Reserva deleted = dao.delete(PagoDAOTest.reserva);
    }

    @Test
    public void test_A_Insert() throws Exception {
        PagoDAO dao = new PagoDAO();
        PagoDAOTest.pago = dao.insert(PagoDAOTest.pago);
        assert !(PagoDAOTest.pago.getNumero() == null);
    }

    @Test
    public void test_D_Update() throws Exception {
        PagoDAO dao = new PagoDAO();
        PagoDAOTest.pago.setMedio("Taroheta");
        PagoDAOTest.pago = dao.update(PagoDAOTest.pago);
        assert PagoDAOTest.pago.getMedio().equals("Taroheta");
    }

    @Test
    public void test_E_Delete() throws Exception {
        PagoDAO dao = new PagoDAO();
        PagoDAOTest.pago = dao.delete(PagoDAOTest.pago);
    }

    @Test
    public void test_C_Select() throws Exception {
        PagoDAO dao = new PagoDAO();
        ArrayList<Pago> ps = dao.selectTodo();
        assert dao.select(ps.get(0).getNumero()).getNumero().equals(ps.get(0).getNumero());
    }

    @Test
    public void test_B_SelectTodo() throws Exception {
        PagoDAO dao = new PagoDAO();
        ArrayList<Pago> ps = dao.selectTodo();
        assert !ps.isEmpty();
    }
}