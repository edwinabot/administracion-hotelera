package Datos.Acceso;

import Datos.Modelo.Reserva;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by edwin on 12/01/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ReservaDAOTest {
    private static Reserva reserva = new Reserva();

    @BeforeClass
    public static void setUp() throws Exception {
        ReservaDAOTest.reserva.setConfirmada(Boolean.FALSE);
        ReservaDAOTest.reserva.setAcompaniantes(2);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        ReservaDAOTest.reserva.setCheckin(df.parse("2016-01-10"));
        ReservaDAOTest.reserva.setCheckout(df.parse("2016-01-16"));
        ReservaDAOTest.reserva.setHabitacion(new HabitacionDAO().selectTodo().get(0));
        ReservaDAOTest.reserva.setHuesped(new PasajeroDAO().selectTodo().get(0));
    }

    @Test
    public void test_A_Insert() throws Exception {
        ReservaDAO dao = new ReservaDAO();
        Reserva insertada = dao.insert(ReservaDAOTest.reserva);
        if (insertada.getNumero() == null) {
            throw new AssertionError();
        } else {
            ReservaDAOTest.reserva = insertada;
        }
    }

    @Test
    public void test_D_Update() throws Exception {
        ReservaDAO dao = new ReservaDAO();
        ReservaDAOTest.reserva.setConfirmada(Boolean.TRUE);
        Reserva modificado = dao.update(reserva);
        if (!(modificado.getNumero().equals(reserva.getNumero()) && modificado.getConfirmada())) {
            throw new AssertionError();
        } else {
            ReservaDAOTest.reserva = modificado;
        }
    }

    @Test
    public void test_E_Delete() throws Exception {
        ReservaDAO dao = new ReservaDAO();
        Reserva deleted = dao.delete(ReservaDAOTest.reserva);
    }

    @Test
    public void test_C_Select() throws Exception {
        ReservaDAO dao = new ReservaDAO();
        ArrayList<Reserva> resultado = dao.selectTodo();
        Reserva registro = dao.select(resultado.get(0).getNumero());
        assert registro.getNumero().equals(resultado.get(0).getNumero());
    }

    @Test
    public void test_B_SelectTodo() throws Exception {
        ReservaDAO dao = new ReservaDAO();
        ArrayList<Reserva> resultado = dao.selectTodo();
        assert !(resultado.isEmpty());
    }
}