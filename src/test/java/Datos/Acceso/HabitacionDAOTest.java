package Datos.Acceso;

import Datos.Modelo.Habitacion;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;

/**
 * Created by edwin on 12/01/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HabitacionDAOTest {

    @Test
    public void test_C_Insert() throws Exception {
        HabitacionDAO dao = new HabitacionDAO();
        Habitacion hab = new Habitacion();
        hab.setNumero(-1);
        hab.setDescripcion("Habitacion test");
        hab.setTarifa(25.65);
        hab.setLimpiar(Boolean.FALSE);
        hab.setMantenimiento(Boolean.FALSE);
        hab.setOcupada(Boolean.FALSE);
        Habitacion insertada = dao.insert(hab);
        assert insertada.getNumero().equals(hab.getNumero());
    }

    @Test
    public void test_B_Select() throws Exception {
        HabitacionDAO dao = new HabitacionDAO();
        ArrayList<Habitacion> resultado = dao.selectTodo();
        Habitacion registro = dao.select(resultado.get(0).getNumero());
        assert registro.getNumero().equals(resultado.get(0).getNumero());
    }

    @Test
    public void test_A_SelectTodo() throws Exception {
        HabitacionDAO dao = new HabitacionDAO();
        ArrayList<Habitacion> resultado = dao.selectTodo();
        assert !(resultado.isEmpty());
    }

    @Test
    public void test_D_Update() throws Exception {
        HabitacionDAO dao = new HabitacionDAO();
        Habitacion registro = dao.select(-1);
        registro.setMantenimiento(Boolean.TRUE);
        Habitacion modificado = dao.update(registro);
        assert modificado.getNumero().equals(registro.getNumero()) && modificado.getMantenimiento();
    }

    @Test
    public void test_E_Delete() throws Exception {
        HabitacionDAO dao = new HabitacionDAO();
        Habitacion registro = dao.select(-1);
        Habitacion deleted = dao.delete(registro);
    }


}